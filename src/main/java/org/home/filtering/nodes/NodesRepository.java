package org.home.filtering.nodes;

import org.home.filtering.core.DataRow;
import org.home.filtering.interfaces.DataRepository;

import java.util.*;
import java.util.stream.Stream;

public class NodesRepository implements DataRepository {
    private Map<Node, Node> nodes;

    @Override
    public void load(List<DataRow> rows) {
        nodes = new HashMap<>();
        List<Node> rawNodes = new ArrayList<>(5);
        List<String> fields = Arrays.asList("country", "company", "model", "year", "capacity");
        for (DataRow row : rows) {
            rawNodes.clear();
            for (String field : fields) {
                Node node = new Node(field, row.getValue(field));
                Node inSet = nodes.get(node);
                if (inSet == null) {
                    nodes.put(node, node);
                } else {
                    node = inSet;
                }
                rawNodes.add(node);
            }

            for (int i = 0; i < rawNodes.size(); i++) {
                for (int j = i + 1; j < rawNodes.size(); j++) {
                    rawNodes.get(i).connectTo(rawNodes.get(j));
                }
            }
        }
    }

    @Override
    public void clear() {
        nodes.clear();
    }

    public Node find(Node node) {
        return nodes.get(node);
    }
}
