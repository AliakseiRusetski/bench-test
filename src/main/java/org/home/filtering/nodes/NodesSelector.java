package org.home.filtering.nodes;


import org.home.filtering.core.FilterCase;
import org.home.filtering.interfaces.DataRepository;
import org.home.filtering.interfaces.DomainSet;
import org.home.filtering.interfaces.Selector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NodesSelector extends Selector {
    final private NodesRepository nodesRepository;

    public NodesSelector(DataRepository nodesRepository) {
        super();
        this.nodesRepository = (NodesRepository) nodesRepository;
    }

    public FilterCase perform(FilterCase filterCase) {
        Set<Node> result = null;
        for (DomainSet filter : filterCase.filters) {
            Set<Node> neighbors = getNodes(filter);

            Set<Node> oneStepConnected = new HashSet<>();
            for (Node neighbor : neighbors) {
                oneStepConnected.addAll(neighbor.connections);
            }
            if (result == null) {
                result = new HashSet<>();
                result.addAll(oneStepConnected);
            } else {
                result.retainAll(oneStepConnected);
            }
        }
        updateDomains(result);

        List<String> filterByNames = new ArrayList<>();
        for (DomainSet filter : filterCase.filters) {
            filterByNames.add(filter.getFieldName());
        }
        List<DomainSet> restDomains = new ArrayList<>();
        for (DomainSet domain : nameToDomain.values()) {
            if (!filterByNames.contains(domain.getFieldName())) {
                restDomains.add(domain);
            }
        }
        return new FilterCase(restDomains);
    }

    private void updateDomains(Set<Node> nodes) {
        for (Node node : nodes) {
            DomainSet domainSet = nameToDomain.get(node.field);
            domainSet.add(node.getValue());
        }
    }

    private Set<Node> getNodes(DomainSet filterCase) {
        Set<Node> result = new HashSet<>();
        for (Object val : filterCase.values()) {
            Node node = new Node(filterCase.getFieldName(), val);
            Node found = nodesRepository.find(node);
            if (found != null) {
                result.add(found);
            }
        }
        return result;
    }
}
