package org.home.filtering.nodes;

import org.home.filtering.core.DataRow;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Node<T> {
    String field;
    T value;

    Set<Node> connections;

    public Node(String field, T value) {
        this.field = field;
        this.value = value;
        connections = new HashSet<>();
    }

    public void connectTo(Node neighbor) {
        if (connections.contains(neighbor)) return;
        connections.add(neighbor);
        neighbor.connectTo(this);
    }

    public String getField() {
        return field;
    }

    public T getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(field) * 100001 + Objects.hashCode(value);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (obj.getClass() != getClass()) return false;
        Node o = (Node) obj;
        return Objects.equals(value, o.value)
                && Objects.equals(field, o.field);
    }
}
