package org.home.filtering;

import org.home.filtering.core.DataRow;
import org.home.filtering.core.DomainSetImpl;
import org.home.filtering.core.FilterCase;
import org.home.filtering.core.TestCase;
import org.home.filtering.interfaces.Generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class DataGenerator implements Generator {

    public static void main(String[] args) {
        DataGenerator dataGenerator = new DataGenerator();
        dataGenerator.tests();
        dataGenerator.tests();
    }

    private static List<TestCase> cache;

    @Override
    public List<TestCase> tests() {
        if (cache != null) {
            return cache;
        }

        synchronized (DataGenerator.class) {
            if (cache != null) {
                return cache;
            }
            List<TestCase> result = new ArrayList<>();
            result.add(new TestCase(getDataset1(), getFilters1()));
            List<DataRow> dataset2 = getDataset2();
            result.add(new TestCase(dataset2, getFilters2(dataset2)));
            List<DataRow> dataset3 = getDataset3();
            result.add(new TestCase(dataset3, getFilters2(dataset3)));
            cache = result;
        }

        return cache;
    }

    public TestCase getTestCase(int index) {
        return tests().get(index);
    }

    private List<FilterCase> getFilters1() {
        List<FilterCase> result = new ArrayList<>();
        result.add(new FilterCase(
                Arrays.asList(
                        new DomainSetImpl<>("model", Arrays.asList("A5"))
                )));
//        fixme: logical problem in NodesSelector
//        result.add(new FilterCase(
//                Arrays.asList(
//                        new DomainSetImpl<>("country", Arrays.asList("USA")),
//                        new DomainSetImpl<>("company", Arrays.asList("Audi"))
//
//        )));
        result.add(new FilterCase(
                Arrays.asList(
                        new DomainSetImpl<>("country", Arrays.asList("DE")),
                        new DomainSetImpl<>("company", Arrays.asList("VM"))
        )));
        result.add(new FilterCase(
                Arrays.asList(
                        new DomainSetImpl<>("company", Arrays.asList("Audi")),
                        new DomainSetImpl<>("country", Arrays.asList("DE", "UK"))
                )));
        return result;
    }

    private List<DataRow> getDataset1() {
        List<DataRow> result;
        result = new ArrayList<>();
        result.add(new DataRow("UK", "VW", "Jetta", 2000, 1.8));
        result.add(new DataRow("UK", "Audi", "A5", 2000, 1.8));
        result.add(new DataRow("UK", "VW", "Polo", 2000, 1.8));
        result.add(new DataRow("USA", "VW", "Jetta", 2000, 1.8));
        result.add(new DataRow("USA", "VW", "Golf", 2000, 1.8));
        result.add(new DataRow("DE", "Audi", "A5", 2000, 1.8));
        result.add(new DataRow("DE", "Audi", "A6", 2000, 1.8));
        return result;
    }

    private List<DataRow> getDataset2() {
        final List<String> countries = generateElements(10, (i) -> "country" + i);
        final List<String> companies = generateElements(10, (i) -> "companies" + i);
        final List<String> models = generateElements(10, (i) -> "models" + i);
        final List<Integer> years = generateElements(10, (i) -> 2000 + i);
        final List<Double> capacity = generateElements(10, (i) -> 2. + i / 10.);
        return getRandom(1000, countries, companies, models, years, capacity);
    }

    private List<DataRow> getDataset3() {
        final List<String> countries = generateElements(40, (i) -> "country" + i);
        final List<String> companies = generateElements(40, (i) -> "companies" + i);
        final List<String> models = generateElements(10, (i) -> "models" + i);
        final List<Integer> years = generateElements(20, (i) -> 2000 + i);
        final List<Double> capacity = generateElements(10, (i) -> 2. + i / 10.);
//        return getRandom(10000000, countries, companies, models, years, capacity);
        return getRandom(100000, countries, companies, models, years, capacity);
    }

    private List<DataRow> getRandom(final int overallLimit, List<String> countries, List<String> companies, List<String> models, List<Integer> years, List<Double> capacity) {
        List<DataRow> result = new ArrayList<>(overallLimit);
        Random random = new Random();
        for (int i = 0; i < overallLimit; i++) {
            result.add(new DataRow(
                    countries.get(random.nextInt(countries.size())),
                    companies.get(random.nextInt(companies.size())),
                    models.get(random.nextInt(models.size())),
                    years.get(random.nextInt(years.size())),
                    capacity.get(random.nextInt(capacity.size()))
            ));
        }
        System.out.println("Generated records: " + result.size());
        return result;
    }

    private <T> List<T> generateElements(int amount, IntFunction mapper) {
        return (List<T>) IntStream.range(0, amount)
                .mapToObj(mapper)
                .collect(Collectors.toList());
    }

    public List<FilterCase> getFilters2(List<DataRow> dataset) {
        List<FilterCase> result = new ArrayList<>();
        result.add(new FilterCase(
                Arrays.asList(
                        new DomainSetImpl<>("model", Arrays.asList(anyElement(dataset).getModel()))
                )));
        result.add(new FilterCase(
                Arrays.asList(
                        new DomainSetImpl<>("model", Arrays.asList(anyElement(dataset).getModel(), anyElement(dataset).getModel())),
                        new DomainSetImpl<>("country", Arrays.asList(anyElement(dataset).getCountry(), anyElement(dataset).getCountry())))
        ));
        result.add(new FilterCase(
                Arrays.asList(
                        new DomainSetImpl<>("company", Arrays.asList(anyElement(dataset).getCompany())),
                        new DomainSetImpl<>("country", Arrays.asList(anyElement(dataset).getCountry(), anyElement(dataset).getCountry())),
                        new DomainSetImpl<>("year", Arrays.asList(anyElement(dataset).getYear(), anyElement(dataset).getYear()))
                )));
        return result;
    }

    Random random = new Random();
    private DataRow anyElement(List<DataRow> dataset) {
        return dataset.get(random.nextInt(dataset.size()));
    }
}
