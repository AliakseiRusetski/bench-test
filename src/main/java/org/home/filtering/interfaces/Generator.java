package org.home.filtering.interfaces;

import org.home.filtering.core.TestCase;

import java.util.List;

public interface Generator {
    List<TestCase> tests();
}
