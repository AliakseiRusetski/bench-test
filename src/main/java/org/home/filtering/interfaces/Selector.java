package org.home.filtering.interfaces;

import org.home.filtering.core.DomainSetImpl;
import org.home.filtering.core.FilterCase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class Selector {
    protected DomainSet<String> country;
    protected DomainSet<String> company;
    protected DomainSet<String> model;
    protected DomainSet<Integer> year;
    protected DomainSet<Double> capacity;
    protected Map<String, DomainSet> nameToDomain = new HashMap<>();

    public Selector() {
        initDomains();
    }

    public void clear() {
        initDomains();
    }

    protected void initDomains() {
        country = new DomainSetImpl<>("country");
        company = new DomainSetImpl<>("company");
        model = new DomainSetImpl<>("model");
        year = new DomainSetImpl<>("year");
        capacity = new DomainSetImpl<>("capacity");
        for (DomainSet it : Arrays.asList(country, company, model, year, capacity)) {
            nameToDomain.put(it.getFieldName(), it);
        }
    }

    public abstract FilterCase perform(FilterCase filters);

    public DomainSet getDomainSet(String name) {
        return nameToDomain.get(name);
    }

}
