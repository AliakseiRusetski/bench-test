package org.home.filtering.interfaces;

import org.home.filtering.core.DataRow;

import java.util.Set;

public interface DomainSet<T> {

    String getFieldName();

    Set<? extends T> values();

    boolean contains(DataRow dataRow);

    void add(DataRow dataRow);

    void add(T data);

    void clear();

    String toString();


}
