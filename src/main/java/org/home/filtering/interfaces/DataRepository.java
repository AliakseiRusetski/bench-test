package org.home.filtering.interfaces;

import org.home.filtering.core.DataRow;

import java.util.List;
import java.util.stream.Stream;

public interface DataRepository {

    void load(List<DataRow> rows);

    void clear();
}
