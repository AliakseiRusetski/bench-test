/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.home.filtering;

import org.home.filtering.bruteforce.StreamRepository;
import org.home.filtering.bruteforce.StreamSelector;
import org.home.filtering.core.FilterCase;
import org.home.filtering.core.TestCase;
import org.home.filtering.interfaces.DataRepository;
import org.home.filtering.interfaces.Selector;
import org.home.filtering.nodes.NodesRepository;
import org.home.filtering.nodes.NodesSelector;
import org.openjdk.jmh.annotations.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class BenchmarkLauncher {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        List<Function<Test, FilterCase>> doFilters = Arrays.asList(Test::doFilter1, Test::doFilter2, Test::doFilter3);
        List<Class<?>> streamTests = Arrays.asList(StreamTest0.class, StreamTest1.class, StreamTest2.class);
        List<Class<?>> nodesTests = Arrays.asList(NodesTest0.class, NodesTest1.class, NodesTest2.class);

        for (int i=0; i<streamTests.size(); i++) {
            Test streamTest = (Test) streamTests.get(i).newInstance();
            streamTest.setup();
            Test nodesTest = (Test) nodesTests.get(i).newInstance();
            nodesTest.setup();
            for (Function<Test, FilterCase> method : doFilters) {
                FilterCase streamResult = method.apply(streamTest);
                FilterCase nodesResult = method.apply(nodesTest);
                System.out.println("streamResult:" + streamResult.toString());
                System.out.println("nodesResult:" + nodesResult.toString());
                assert streamResult.equals(nodesResult);
            }
        }
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 10, time = 1)
    @Fork(1)
    @State(Scope.Benchmark)
    public static abstract class Test {
        Selector selector;
        TestCase testCase;

        @Setup(Level.Trial)
        public void setup() {
            System.out.println("Setup: " + this.getClass().getName());
            testCase = new DataGenerator().getTestCase(getTestIndex());
            DataRepository repository = createRepository(testCase);
            selector = createSelector(repository);
        }

        abstract protected int getTestIndex();

        abstract protected DataRepository createRepository(TestCase testCase);

        abstract protected Selector createSelector(DataRepository dataRepository);

        @Benchmark
        public FilterCase doFilter1() {
            selector.clear();
            return selector.perform(testCase.filters.get(0));
        }

        @Benchmark
        public FilterCase doFilter2() {
            selector.clear();
            return selector.perform(testCase.filters.get(1));
        }

        @Benchmark
        public FilterCase doFilter3() {
            selector.clear();
            return selector.perform(testCase.filters.get(2));
        }
    }

    @State(Scope.Benchmark)
    abstract static class StreamTest extends Test {
        @Override
        protected DataRepository createRepository(TestCase testCase) {
            StreamRepository streamRepository = new StreamRepository();
            streamRepository.load(testCase.data);
            return streamRepository;
        }

        @Override
        protected Selector createSelector(DataRepository dataRepository) {
            return new StreamSelector(dataRepository);
        }
    }

    @State(Scope.Benchmark)
    public static class StreamTest0 extends StreamTest {
        @Override
        protected int getTestIndex() {
            return 0;
        }
    }

    @State(Scope.Benchmark)
    public static class StreamTest1 extends StreamTest {
        @Override
        protected int getTestIndex() {
            return 1;
        }
    }

    @State(Scope.Benchmark)
    public static class StreamTest2 extends StreamTest {
        @Override
        protected int getTestIndex() {
            return 2;
        }
    }

    @State(Scope.Benchmark)
    abstract static class NodesTest extends Test {
        @Override
        protected DataRepository createRepository(TestCase testCase) {
            NodesRepository nodes = new NodesRepository();
            nodes.load(testCase.data);
            return nodes;
        }

        @Override
        protected Selector createSelector(DataRepository dataRepository) {
            return new NodesSelector(dataRepository);
        }
    }

    @State(Scope.Benchmark)
    public static class NodesTest0 extends NodesTest {
        @Override
        protected int getTestIndex() {
            return 0;
        }
    }

    @State(Scope.Benchmark)
    public static class NodesTest1 extends NodesTest {
        @Override
        protected int getTestIndex() {
            return 1;
        }
    }

    @State(Scope.Benchmark)
    public static class NodesTest2 extends NodesTest {
        @Override
        protected int getTestIndex() {
            return 2;
        }
    }
}
