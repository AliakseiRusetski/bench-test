package org.home.filtering.core;

import org.home.filtering.interfaces.DomainSet;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class FilterCase {
    public List<DomainSet> filters;

    public FilterCase(List<DomainSet> domainSets) {
        filters = domainSets;
    }

    @Override
    public String toString() {
        return filters.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        FilterCase compareTo = (FilterCase) obj;
        HashSet<DomainSet> domainSets = new HashSet<>();
        domainSets.addAll(filters);
        domainSets.addAll(compareTo.filters);
        return domainSets.size() == filters.size();
    }

    public static void main(String[] args) {
        FilterCase a = new FilterCase(Arrays.asList(
                new DomainSetImpl("model", Arrays.asList("modelA","modelB","modelC")),
                new DomainSetImpl("country", Arrays.asList("countryA","countryB","countryC"))
        ));

        FilterCase b = new FilterCase(Arrays.asList(
                new DomainSetImpl("country", Arrays.asList("countryC", "countryA","countryB")),
                new DomainSetImpl("model", Arrays.asList("modelB", "modelA","modelC"))
        ));

        FilterCase c = new FilterCase(Arrays.asList(
                new DomainSetImpl("country", Arrays.asList("countryC", "countryD","countryB")),
                new DomainSetImpl("model", Arrays.asList("modelB", "modelA","modelC"))
        ));

        FilterCase d = new FilterCase(Arrays.asList(
                new DomainSetImpl("model", Arrays.asList("modelB", "modelA","modelC"))
        ));

        FilterCase capA = new FilterCase(Arrays.asList(
                new DomainSetImpl("capacity", Arrays.asList(1.2, 1.3,1.4))
        ));
        FilterCase capB = new FilterCase(Arrays.asList(
                new DomainSetImpl("capacity", Arrays.asList(1.4, 1.2, 1.3))
        ));

        assert a.equals(b);
        assert !a.equals(c);
        assert !b.equals(c);
        assert !a.equals(d);
        assert !d.equals(c);
        assert capA.equals(capB);
    }
}
