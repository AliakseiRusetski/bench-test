package org.home.filtering.core;

import java.util.List;
import java.util.Objects;

public class DataRow {
    private String country;
    private String company;
    private String model;
    private Integer year;
    private Double capacity;

    public DataRow() {
    }

    public DataRow(String country, String company, String model, Integer year, Double capacity) {
        this.country = country;
        this.company = company;
        this.model = model;
        this.year = year;
        this.capacity = capacity;
    }

    public String getCountry() {
        return country;
    }

    public String getCompany() {
        return company;
    }

    public String getModel() {
        return model;
    }

    public Integer getYear() {
        return year;
    }

    public Double getCapacity() {
        return capacity;
    }

    @Override
    public int hashCode() {
        int result;
        result = Objects.hashCode(country);
        result = result * 41 + Objects.hashCode(company);
        result = result * 41 + Objects.hashCode(model);
        result = result * 41 + Objects.hashCode(year);
        result = result * 41 + Objects.hashCode(capacity);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (obj.getClass() != getClass()) return false;
        DataRow o = (DataRow) obj;
        return Objects.equals(country, o.country)
                && Objects.equals(company, o.company)
                && Objects.equals(model, o.model)
                && Objects.equals(year, o.year)
                && Objects.equals(capacity, o.capacity);
    }

    public <T> T getValue(String fieldName) {
        switch (fieldName) {
            case "country": return (T) country;
            case "company": return (T) company;
            case "model": return (T) model;
            case "year": return (T) year;
            case "capacity": return (T) capacity;
            default:
                System.err.println("unknown field \"" + fieldName + "\"");
        }
        return null;
    }
}
