package org.home.filtering.core;

import org.home.filtering.interfaces.DomainSet;

import java.util.*;

public class DomainSetImpl<T> implements DomainSet<T> {
    final String fieldName;
    final Set<T> storage;

    public DomainSetImpl(String fieldName) {
        this.fieldName = fieldName;
        this.storage = new HashSet<>();
    }

    public DomainSetImpl(String fieldName, List<T> values) {
        this.fieldName = fieldName;
        this.storage = new HashSet<>();
        this.storage.addAll(values);
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public Set<? extends T> values() {
        return storage;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new DomainSetImpl<T>(fieldName, new ArrayList<>(storage));
    }

    @Override
    public boolean contains(DataRow dataRow) {
        return storage.contains(dataRow.getValue(fieldName));
    }

    @Override
    public void add(DataRow dataRow) {
        storage.add(dataRow.getValue(fieldName));
    }

@Override
    public void add(T data) {
        storage.add(data);
    }

    @Override
    public void clear() {
        storage.clear();
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(fieldName).append("->");
        for (T t : storage) {
            stringBuffer.append(t.toString()).append(", ");
        }
        stringBuffer.replace(stringBuffer.length() - 2, stringBuffer.length(), "; ");
        return stringBuffer.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(fieldName) * 100001 + Objects.hashCode(storage);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        DomainSetImpl compareTo = (DomainSetImpl) obj;
        if (!fieldName.equals(compareTo.fieldName)) return false;
        return storage.containsAll(compareTo.storage) && compareTo.storage.containsAll(storage);
    }
}
