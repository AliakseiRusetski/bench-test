package org.home.filtering.core;

import java.util.List;

public class TestCase {
    public List<DataRow> data;
    public List<FilterCase> filters;

    public TestCase(List<DataRow> data, List<FilterCase> filters) {
        this.data = data;
        this.filters = filters;
    }
}
