package org.home.filtering.bruteforce;


import org.home.filtering.core.*;
import org.home.filtering.interfaces.DataRepository;
import org.home.filtering.interfaces.DomainSet;
import org.home.filtering.interfaces.Selector;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamSelector extends Selector {
    private final StreamRepository dataRepository;

    public StreamSelector(DataRepository dataRepository) {
        super();
        this.dataRepository = (StreamRepository)dataRepository;
    }

    private void updateDomains(Iterable<DataRow> dataCollection) {
        for (Map.Entry<String, DomainSet> it : nameToDomain.entrySet()) {
            it.getValue().clear();
        }
        dataCollection.forEach((DataRow r) -> {
            for (DomainSet it : nameToDomain.values()) {
                it.add(r);
            }
        });
    }

    public FilterCase perform(FilterCase filterCase) {
        List<DomainSet> filters = filterCase.filters;
        Stream<DataRow> stream = dataRepository.stream();
        for (DomainSet filter : filters) {
            stream = stream.filter(filter::contains);
        }
        updateDomains(stream.collect(Collectors.toList()));
        List<String> filterNames = filters.stream().map(DomainSet::getFieldName).collect(Collectors.toList());
        List<DomainSet> result = new ArrayList<>();
        for (String name : nameToDomain.keySet()) {
            if (filterNames.contains(name)) continue;
            result.add(nameToDomain.get(name));
        }
        return new FilterCase(result);
    }
}
