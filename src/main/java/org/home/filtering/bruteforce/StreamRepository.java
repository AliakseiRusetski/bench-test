package org.home.filtering.bruteforce;

import org.home.filtering.interfaces.DataRepository;
import org.home.filtering.core.DataRow;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamRepository implements DataRepository {
    private List<DataRow> rows;

    @Override
    public void load(List<DataRow> rows) {
        this.rows = new ArrayList<>(rows.size());
        this.rows.addAll(rows);
    }

    Stream<DataRow> stream() {
        return rows.stream();
    }

    @Override
    public void clear() {
        rows.clear();
    }
}
